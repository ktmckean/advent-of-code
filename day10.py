import math

starMap = []

class vector:
	x = 0
	y = 0
	
	def __init__(self, x, y):
		self.x = x
		self.y = y
		
	def __str__(self):
		return str([self.x, self.y])
		
	def __add__(self, other):
		x = self.x + other.x
		y = self.y + other.y
		return vector(x,y)

	def __sub__(self, other):
		x = self.x - other.x
		y = self.y - other.y
		return vector(x,y)

	def __mod__(self, other):
		if self == other:
			return self
			
		if other.x == 0 and self.x == 0:
			xmod = 0
		else:
			xmod = self.x % other.x
			
		if other.y == 0 and self.y == 0:
			ymod = 0
		else:
			ymod = self.y % other.y
		return vector(xmod,ymod)
		
	def timesScalar(self, int):
		vec = vector(self.x, self.y)
		vec.x *= int
		vec.y *= int
		return vec
		
	def __gt__(self, other):
		return self.magSquared() > other.magSquared()

	def __lt__(self, other):
		return self.magSquared() < other.magSquared()
	
	def __eq__(self, other):
		return (self.x == other.x and self.y == other.y)

	def __ne__(self, other):
		return (self.x != other.x or self.y != other.y)
		
	def magSquared(self):
		return self.x**2 + self.y**2
		
	def mag(self):
		return math.sqrt(self.magSquared())
	
	def reduce(self):
		vec = vector(self.x, self.y)
		if abs(vec.x) == abs(vec.y):
			vec.x = 1 * sign(vec.x)
			vec.y = 1 * sign(vec.y)
		elif vec.x == 0:
			vec.y = 1 * sign(vec.y)	
		elif vec.y == 0:
			vec.x = 1 * sign(vec.x)
		else:
			factor = gcd(vec.x, vec.y)
			vec.x = int(vec.x / factor)
			vec.y = int(vec.y / factor)
		return vec
		
	def sharesSightLineWith(self, other):
		return self.reduce() == other.reduce()
		
	# Nice work but don't use this
	def isCollinearWith(self, other):
		return areCollinear(self, other)
		
#Counts oppotites.  Not what we want
def areCollinear(vec1, vec2):
	reduced = vec1.reduce()
	if vec1.times(-1) == vec2:
		return False
	if vec2 % reduced == vector(0,0):
		return True
		
def sign(num):
	assert num != 0
	return 1*int(num>0) -1*int(num<0)
		
def gcd(a, b):
	gcd = 1
	for num in range(1, max(abs(a)+1,abs(b)+1)):
		if a % num == 0 and b % num == 0:
			gcd = num
	return gcd
	
def loadInput(filename):
	global starMap
	starMap = []
	file = open(filename)
	for line in file:
		starMap.append(list(line.strip()))
	return starMap
	
def getAsteroidCoords():
	asteroids = []
	for yPos, row in enumerate(starMap):
		for xPos, val in enumerate(row):
			if val == '#':
				asteroids.append(vector(xPos, yPos))
	return asteroids

def getSeenAsteroids(asteroids, base):
	sightLines = []
	for asteroid in asteroids:
		if asteroid == base:
			continue
			
		asteroid -= base
			
		seen = True
		for i,seen in enumerate(sightLines):
			if asteroid.sharesSightLineWith(seen):
				if asteroid.magSquared() < seen.magSquared():
					sightLines[i] = asteroid
				seen = False
				break				
		if seen:
			sightLines.append(asteroid)
			
	for i,seen in enumerate(sightLines):
		sightLines[i] += base
		
	return sightLines




# Slower, but every seen asteroid stays identifiable by its coordinates
def numSeen(asteroids, base):
	sightLines = []
	for asteroid in asteroids:
		if asteroid == base:
			#print('skipping base')
			continue
			
		
		asteroid -= base
			
		seen = True
		for i,seen in enumerate(sightLines):
			if asteroid.sharesSightLineWith(seen):
				#if base == vector(6,3):
				#	print("sight of "+str(max(seen, asteroid))+' blocked by '+str(min(seen,asteroid)))
				if asteroid.magSquared() < seen.magSquared():
					sightLines[i] = asteroid
				seen = False
				break
				
				
		if seen:
			#print('base '+str(base)+' saw asteroid: '+str(asteroid))
			sightLines.append(asteroid)
	
	#if base == vector(6, 3):
	#	for asteroid in sightLines:
	#		print('base '+str(base)+" saw asteroid: "+str(asteroid)+' (reduced: '+str(asteroid.reduce())+')')
		
	return len(sightLines)

# Faster, but doesn't preserve information on the asteroids' coordinates
def numSeen2(asteroids, base):				
	seen = []
	for asteroid in asteroids:
		if asteroid == base:
			continue
	
		asteroid -= base
		sightLine = asteroid.reduce()
		if sightLine not in seen:
			seen.append(sightLine)
	
	return len(seen)
	
def numSeenFromAsteroid(asteroid, asteroids):	
	return numSeen(asteroids, asteroid)
	
def isSeen(asteroid, base, asteroids):
	sightLine = (asteroid - base).reduce()
	for a in asteroids:
		if a == base:
			continue
		a = a - base
		flag = False
		if a.reduce() == sightLine:
			print(str(a)+' shares sightLine with '+str(asteroid-base))
			if a < (asteroid - base):
				return False
	return True
	

def doPartOne():
	loadInput('day10input1.txt')
	asteroids = getAsteroidCoords()
	
	maxSeen = 0
	best = vector(0,0)
	
	for i, asteroid in enumerate(asteroids):
		base = asteroid
			
		num = numSeen(asteroids, base)
		if num > maxSeen:
			maxSeen = num
			best = asteroid
		print('Saw '+str(num)+' from asteroid '+str(asteroid)+', '+str(i+1)+' of '+str(len(asteroids)))
	
	print(str(maxSeen)+', '+str(best))
	

def vaporizeAllSeen(seen):
	global starMap
	numVaporized = len(seen)
	for asteroid in seen:
		starMap[asteroid.y][asteroid.x] = '.'
	return numVaporized

#def findSeenAlongAxis(asteroids, sight):
		
def assignToRelativeQuadrants(seen, base):
	q1 = []
	q2 = []
	q3 = []
	q4 = []
	for a in seen:
		a -= base
		#Starting from top right going clockwise
		#y axis is inverted
		if a.x >= 0 and a.y < 0:
			q1.append(a)
		elif a.x > 0 and a.y >= 0:
			q2.append(a)
		elif a.x <= 0 and a.y > 0:
			q3.append(a)
		elif a.x < 0 and a.y <= 0:
			q4.append(a)
	return [q1,q2,q3,q4]
	#for asteroid in seen:
	
# Quadrant starts at (0,1) and increases clockwise
def addAngleToPointsInQuadrant(quadrant, qNum):	
	if qNum == 0:
		start = vector(0,-1)
	elif qNum == 1:
		start = vector(1,0)
	elif qNum == 2:
		start = vector(0,1)	
	elif qNum == 3:
		start = vector(-1,0)
	for i, a in enumerate(quadrant):
		dotProd = start.x*a.x + start.y*a.y
		assert abs(dotProd) <= abs(start.mag())*abs(a.mag())
		# It's okay to do mod pi/2 because all of our vectors are at angles <= pi/2 with start
		angle = math.acos( dotProd / (start.mag()*a.mag()) )
		quadrant[i] = [a, angle]
	return quadrant
		
def vaporize(asteroid):
	global starMap
	#starMap is organized by row, then column
	starMap[asteroid.y][asteroid.x] = '.'
		
def vaporizeClockwise(base):		
	count = 0
	
	while count < 200:
		asteroids = getAsteroidCoords()
		seen = getSeenAsteroids(asteroids, base)
		quads = assignToRelativeQuadrants(seen, base)
		#quads contain relative points


		for i,q in enumerate(quads):
			if len(q) ==0:
				continue
			q = addAngleToPointsInQuadrant(q,i)
			q.sort(key=lambda aa: aa[1])
			for aa in q:
				aa[0] += base	# We're done with relative position
				vaporize(aa[0])
				count += 1
				if count == 200:
					return aa[0]

def main():
	'''
	loadInput('day10input2.txt')
	base = vector(11,13)	# 200th is [8,2]
	end  = vaporizeClockwise(base)
	print(end)
	exit()
	'''
	
	loadInput('day10input1.txt')
	base = vector(20,21)
	twoHundredth = vaporizeClockwise(base)
	
	print(twoHundredth)
	print("congrats! asteroid "+str(twoHundredth+base)+' won the prize!')
	exit()
	
	numVaporized = 0
	c = 0
	while(numVaporized < 200):
		asteroids = getAsteroidCoords()
		seen = getSeenAsteroids(asteroids, base)
		# Remember, seen is relative to base
		numVaporized += vaporizeAllSeen(seen)
		c+=1
		
	print([c, numVaporized])

	
	

if __name__=='__main__':
	main()