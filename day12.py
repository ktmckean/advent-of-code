from vector3d import vector3d as vec
import time

class planet:
	p = vec(0,0,0)
	v = vec(0,0,0)
	
	def __init__(self, x,y,z):
		self.p = vec(x,y,z)
		self.v = vec(0,0,0)
		
	def __eq__(self, other):
		return self.p == other.p and self.v == other.v
		
	def __str__(self):
		return "p="+str(self.p)+',\t v='+str(self.v)
	
	# Addition here doesn't really make sense
	#def __min__(self, other):
	#	return self.p - other.p
	
	def pot(self):
		return abs(self.p.x) + abs(self.p.y) + abs(self.p.z)
		
	def kin(self):
		return abs(self.v.x) + abs(self.v.y) + abs(self.v.z)
	
	def energy(self):
		return self.pot() * self.kin()


class system:
	bodies = []
	history = {}
	
	def __init__(self, bodies):
		self.bodies = bodies.copy()
		
	def __str__(self):
		s = ""
		for b in self.bodies:
			s += str(b) + '\n'
		return s

	def getStateKey(self):
		state = ()
		for b in self.bodies:
			state+=b.p.tup()
			state+=b.v.tup()
		return state

	def record(self, state = ()):
		if state != ():
			self.history[state]=1
		for b in self.bodies:
			state+=b.p.tup()
		self.history[state] = 1

	def fastUpdate(self):
		updates = [vec(0,0,0)]*4
		# xOrder = {}
		# yOrder = {}
		# zOrder = {}
		#
		# for b in self.bodies:
		# 	xOrder[b.p.tup()+b.v.tup()] = b

		for i,b in enumerate(self.bodies):
			for j,b2 in enumerate(self.bodies[i+1:]):
				update = (b2.p-b.p).signs()
				self.bodies[i].v += update
				self.bodies[i+j+1].v -= update
		for i,b in enumerate(self.bodies):
			self.bodies[i].p += self.bodies[i].v

	def update(self):
		updates = []
		for b in self.bodies:
			update = vec(0,0,0)
			for b2 in self.bodies:
				if b==b2:
					continue
				update += (b2.p-b.p).signs()
			updates.append(update)
		for i,b in enumerate(self.bodies):
			self.bodies[i].v += updates[i]
			self.bodies[i].p += self.bodies[i].v
	
	def updateT(self, times):
		start = time.time()
		for i in range(times):
			self.update()
		print(str(times)+" updates in "+str(time.time()-start)+' seconds')

	def runUntilRepeat(self):
		start = time.time()
		count = 0
		marked = 1
		while True:
			state = self.getStateKey()
			if state not in self.history:
				self.record(state)
				#self.update()
				self.fastUpdate()
				count+=1
				if count > marked:
					print("updated "+str(marked)+" times\t\ttime: "+str(time.time()-start))
					marked *=10
				continue
			break
		return count

	def energy(self):
		e = 0
		for b in self.bodies:
			e += b.energy()
		return e


def main():
	# io = planet(-10,-10,-13)
	# callisto = planet(5,5,-9)
	# europa = planet(3,8,-16)
	# ganymede = planet(1,3,-3)

	# io = planet(-1,0,2)
	# callisto = planet(2,-10,-7)
	# europa = planet(4,-8,8)
	# ganymede = planet(3,5,-1)
	
	io = planet(-8,-10,0)
	callisto = planet(5,5,10)
	europa = planet(2,-7,3)
	ganymede = planet(9,-8,-3)
	
	moons = []
	moons.append(io)
	moons.append(callisto)
	moons.append(europa)
	moons.append(ganymede)
		
	jovian = system(moons)

	print(jovian.runUntilRepeat())

	# jovian.updateT(2772)
	# print(jovian)
	# print(jovian.getStateKey())

	
	
	
	
if __name__=='__main__':
	main()