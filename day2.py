const = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,2,19,13,23,1,23,10,27,1,13,27,31,2,31,10,35,1,35,9,39,1,39,13,43,1,13,43,47,1,47,13,51,1,13,51,55,1,5,55,59,2,10,59,63,1,9,63,67,1,6,67,71,2,71,13,75,2,75,13,79,1,79,9,83,2,83,10,87,1,9,87,91,1,6,91,95,1,95,10,99,1,99,13,103,1,13,103,107,2,13,107,111,1,111,9,115,2,115,10,119,1,119,5,123,1,123,2,127,1,127,5,0,99,2,14,0,0]
code = [1,0,0,3,1,1,2,3,1,3,4,3,1,5,0,3,2,6,1,19,2,19,13,23,1,23,10,27,1,13,27,31,2,31,10,35,1,35,9,39,1,39,13,43,1,13,43,47,1,47,13,51,1,13,51,55,1,5,55,59,2,10,59,63,1,9,63,67,1,6,67,71,2,71,13,75,2,75,13,79,1,79,9,83,2,83,10,87,1,9,87,91,1,6,91,95,1,95,10,99,1,99,13,103,1,13,103,107,2,13,107,111,1,111,9,115,2,115,10,119,1,119,5,123,1,123,2,127,1,127,5,0,99,2,14,0,0]

def restoreGravityAssist():
	code [1] = 12
	code [2] = 2
	return
	
def getOpCode(i):
	#print(str(i) +": " +str(code[i:i+4]))
	#print("whole code: " + str(code))
	if i+3 < len(code):
		return code[i:i+4]
	elif i < len(code):
		return code[i]
	else:
		return [99]
	
	
def executeOpCode(op):
	if len(op)==1:
		return (code[0])
	if(op[3] >= len(code)):
		return -1
		
	#print(op[3])
	#print(code[:20])
	
	if op[0] == 1:
		code[op[3]] = code[op[1]]+code[op[2]]
	elif op[0] == 2:
		code[op[3]] = code[op[1]]*code[op[2]]
	elif op[0] == 99:
		return code[0]
	
	return 0	# All normal
		
def runProgram(i,j):
	#print(code)
	pc = 0
	code[1] = i
	code[2] = j
	
	while pc+3 < len(code):
		op = getOpCode(pc)
		result = executeOpCode(op)
		
		if result < 0:
			print("out of bounds destination for opcode " + str(op) + ', instruction at ' + str(pc))
			return -1
		elif result==99:
			return code[0]
		pc+=4
		
	return code[0]
	
def resetCode():
	for i in range(len(code)):
		code[i] = const[i]
	return

		
def main():
	#runProgram(18,20)
	#exit()

	#print(getOpCode(0))
	#print(getOpCode(4))
	#print(getOpCode(8))	
	#print(const)
	#runProgram(const)
	#exit(0)

	print ('max = ' + str(len(const)))
	for i in range(0,100):
		for j in range(0,100):
			resetCode()
			#print([i,j])
			code[1] = i
			code[2] = j
			result = runProgram(i,j)
			if(result<0):
				print("North Pole, we had a problem")
				exit()
			
			if 19690720 == result:
				print(str([i,j]))
				exit()
	print('false')
				
	
	
if __name__=="__main__":
	main()