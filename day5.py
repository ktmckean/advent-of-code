mem = [3,225,1,225,6,6,1100,1,238,225,104,0,1101,11,91,225,1002,121,77,224,101,-6314,224,224,4,224,1002,223,8,223,1001,224,3,224,1,223,224,223,1102,74,62,225,1102,82,7,224,1001,224,-574,224,4,224,102,8,223,223,1001,224,3,224,1,224,223,223,1101,28,67,225,1102,42,15,225,2,196,96,224,101,-4446,224,224,4,224,102,8,223,223,101,6,224,224,1,223,224,223,1101,86,57,225,1,148,69,224,1001,224,-77,224,4,224,102,8,223,223,1001,224,2,224,1,223,224,223,1101,82,83,225,101,87,14,224,1001,224,-178,224,4,224,1002,223,8,223,101,7,224,224,1,223,224,223,1101,38,35,225,102,31,65,224,1001,224,-868,224,4,224,1002,223,8,223,1001,224,5,224,1,223,224,223,1101,57,27,224,1001,224,-84,224,4,224,102,8,223,223,1001,224,7,224,1,223,224,223,1101,61,78,225,1001,40,27,224,101,-89,224,224,4,224,1002,223,8,223,1001,224,1,224,1,224,223,223,4,223,99,0,0,0,677,0,0,0,0,0,0,0,0,0,0,0,1105,0,99999,1105,227,247,1105,1,99999,1005,227,99999,1005,0,256,1105,1,99999,1106,227,99999,1106,0,265,1105,1,99999,1006,0,99999,1006,227,274,1105,1,99999,1105,1,280,1105,1,99999,1,225,225,225,1101,294,0,0,105,1,0,1105,1,99999,1106,0,300,1105,1,99999,1,225,225,225,1101,314,0,0,106,0,0,1105,1,99999,1008,677,226,224,1002,223,2,223,1006,224,329,101,1,223,223,8,226,677,224,102,2,223,223,1005,224,344,101,1,223,223,1107,226,677,224,102,2,223,223,1006,224,359,101,1,223,223,1007,226,226,224,102,2,223,223,1006,224,374,101,1,223,223,7,677,677,224,102,2,223,223,1005,224,389,1001,223,1,223,108,677,677,224,1002,223,2,223,1005,224,404,101,1,223,223,1008,226,226,224,102,2,223,223,1005,224,419,1001,223,1,223,1107,677,226,224,102,2,223,223,1005,224,434,1001,223,1,223,1108,677,677,224,102,2,223,223,1006,224,449,1001,223,1,223,7,226,677,224,102,2,223,223,1005,224,464,101,1,223,223,1008,677,677,224,102,2,223,223,1005,224,479,101,1,223,223,1007,226,677,224,1002,223,2,223,1006,224,494,101,1,223,223,8,677,226,224,1002,223,2,223,1005,224,509,101,1,223,223,1007,677,677,224,1002,223,2,223,1006,224,524,101,1,223,223,107,226,226,224,102,2,223,223,1006,224,539,101,1,223,223,107,226,677,224,102,2,223,223,1005,224,554,1001,223,1,223,7,677,226,224,102,2,223,223,1006,224,569,1001,223,1,223,107,677,677,224,1002,223,2,223,1005,224,584,101,1,223,223,1107,677,677,224,102,2,223,223,1005,224,599,101,1,223,223,1108,226,677,224,102,2,223,223,1006,224,614,101,1,223,223,8,226,226,224,102,2,223,223,1006,224,629,101,1,223,223,108,226,677,224,102,2,223,223,1005,224,644,1001,223,1,223,108,226,226,224,102,2,223,223,1005,224,659,101,1,223,223,1108,677,226,224,102,2,223,223,1006,224,674,1001,223,1,223,4,223,99,226]

#mem = [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]


#mem = [3,9,8,9,10,9,4,9,99,-1,8]	# print 1 if input == 8, else print 0	# position
#mem = [3,9,7,9,10,9,4,9,99,-1,8]	# print 1 if input < 8, else print 0	# position
#mem = [3,3,1108,-1,8,3,4,3,99]	# print 1 if input == 8, else print 0	# position
#mem = [3,3,1107,-1,8,3,4,3,99]	# print 1 if input < 8, else print 0	# position

#mem = [3,12,6,12,15,1,13,14,13,4,13,99,-1,0,1,9]	# print 0 if input == 0, else print 1	# position
#mem = [3,3,1105,-1,9,1101,0,0,12,4,12,99,1]		# print 0 if input == 0, else print 1	# immediate


# print 999 if input < 8, 1000 if input == 8, and 1001 if input > 8
#mem = [3,21,1008,21,8,20,1005,20,22,107,8,21,20,1006,20,31,1106,0,36,98,0,0,1002,21,125,20,4,20,1105,1,46,104,999,1105,1,46,1101,1000,1,20,4,20,1105,1,46,98,99]

#Global program counter
pc = 0


#Address is a memory index.  Memory at that index contains either the value (mode 1) or the address of the value (0)
def getArg(address, mode):
	global mem
	#print([mode,address])
	if mode ==0:	#
		#print('getting arg at '+str(mem[address])+"; it's "+str(mem[mem[address]]))
		return mem[mem[address]]
	elif mode ==1:
		#print('Using arg at '+str(address)+"; it's "+str(mem[address]))
		return mem[address]
	else:
		print("Error: Mode must be 0 or 1.  Mode was "+str(mode))
		exit()

def getOpAndArgs():
	global pc
	global mem
	op = mem[pc]
	operation = op%100	#two digits
	arg1mode = int(op/100)%10
	arg2mode = int(op/1000)%10
	#arg3mode = int(op/10000)%10	# never immediate
			
	if operation == 1 \
	or operation == 2 \
	or operation == 7 \
	or operation == 8:	#add, multiply, less than, equals
		assert pc+3 < len(mem)
		arg1 = getArg(pc+1, arg1mode)
		arg2 = getArg(pc+2, arg2mode)
		arg3 = getArg(pc+3, 1)	# Writing always uses the arg at pc+3 directly
		return [operation, arg1, arg2, arg3]
			
	#No more arguments for ops 3 and 4
	elif operation == 3:
		assert pc+1 < len(mem)
		arg1 = getArg(pc+1, 1)	# always use arg at pc + 1 directly
		return [operation, arg1]

	elif operation == 4:
		assert pc+1 < len(mem)
		arg1 = getArg(pc+1, arg1mode)
		return [operation, arg1]
		
	elif operation == 5 or operation == 6:	#jump if true, jump if false
		assert pc+2 < len(mem)
		arg1 = getArg(pc+1, arg1mode)
		arg2 = getArg(pc+2, arg2mode)
		return [operation, arg1, arg2]
	
	elif operation == 99:
		return [operation]
	
	else:
		print("Error: Operations 1-8 and 99 permitted.  Operation was "+str(op)) #str(operation))
		print(mem[:pc+4])

def executeOperation(op, input = []):
	global mem
	global pc
	output = []
	if op[0] == 1:
		#if(pc > 206):
		#print("Writing value "+str(op[1]+op[2]) +" to address "+str(op[3]))
		mem[op[3]] = op[1]+op[2]
		pc += len(op)
	elif op[0] == 2:
		#if(pc > 206):
		#print("Writing value "+str(op[1]*op[2]) +" to address "+str(op[3]))
		mem[op[3]] = op[1]*op[2]
		pc += len(op)
	elif op[0] == 3:
		assert len(input) > 0
		#if(pc > 206):
		#print("Writing value "+str(input[0]) +" to address "+str(op[1]))
		mem[op[1]] = input[0]
		pc += len(op)
	elif op[0] == 4:
		output.append(op[1])
		pc += len(op)
	elif op[0] == 5:
		if op[1] != 0:
			#print('Is '+str(op[1])+' non-zero?  It is! Jumping to '+str(op[2]))
			pc = op[2]
		else:
			#print('Is '+str(op[1])+" non-zero?  It isn't. Advancing to next instruction.")
			pc += len(op)
	elif op[0] == 6:
		if op[1] == 0:
			#print('Is '+str(op[1])+' equal to 0?  It is! Jumping to '+str(op[2]))
			pc = op[2]
		else:
			#print('Is '+str(op[1])+" equal to 0?  It isn't. Advancing to next instruction.")
			pc += len(op)
	elif op[0] == 7:
		#print ('Comparing. is '+str(op[1])+' less than '+str(op[2])+'?  ', end='')
		if op[1] < op[2]:
			#print("It is! Writing 1 to address "+str(op[3]))
			mem[op[3]] = 1
		else:
			#print("It isn't. Writing 0 to address "+str(op[3]))
			mem[op[3]] = 0
		pc += len(op)
	elif op[0] == 8:
		#print("comparing: "+str(op[1])+" and "+str(op[2])+", are they equal?  ", end ='')
		if op[1] == op[2]:
			#print("They are; writing 1 to address "+str(op[3]))
			mem[op[3]] = 1
		else:
			#print("They aren't; writing 0 to address "+str(op[3]))
			mem[op[3]] = 0
		pc += len(op)
	elif op[0] == 99:
		exit()
		output.append(mem[0])
		pc += len(op)
	
	return output	# All normal
		
def runProgram(input):
	global pc
	global mem
	#print(len(mem))
	
	while pc < len(mem):
		#print(mem[:min(24,pc+4)])
		op = getOpAndArgs()
		#print("pc: "+str(pc)+", op: "+str(op))
		#print("mem: "+str(mem[pc:pc+8]))
		
		if op[0] == 3:
			output = executeOperation(op, input)
		else:
			output = executeOperation(op)
		
		if len(output) > 0:
			print("output: "+str(output)+", pc at "+str(pc))
			
		if op[0]==99:
			exit()
		
	return []
	
def resetMem():
	print("reset mem:")
	for i in range(len(mem)):
		mem[i] = const[i]
	return

		
def main():
	#resetMem()
	input = [5]	
	runProgram(input)
	
	
if __name__=="__main__":
	main()