from intCodeComputer import computer as comp


ampCode = [3,8,1001,8,10,8,105,1,0,0,21,46,67,76,97,118,199,280,361,442,99999,3,9,1002,9,3,9,101,4,9,9,102,3,9,9,1001,9,3,9,1002,9,2,9,4,9,99,3,9,102,2,9,9,101,5,9,9,1002,9,2,9,101,2,9,9,4,9,99,3,9,101,4,9,9,4,9,99,3,9,1001,9,4,9,102,2,9,9,1001,9,4,9,1002,9,5,9,4,9,99,3,9,102,3,9,9,1001,9,2,9,1002,9,3,9,1001,9,3,9,4,9,99,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,99,3,9,102,2,9,9,4,9,3,9,101,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,102,2,9,9,4,9,3,9,101,1,9,9,4,9,3,9,101,2,9,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,1,9,4,9,3,9,101,1,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1001,9,1,9,4,9,3,9,1002,9,2,9,4,9,3,9,1001,9,1,9,4,9,99,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1002,9,2,9,4,9,3,9,101,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,101,1,9,9,4,9,3,9,102,2,9,9,4,9,3,9,102,2,9,9,4,9,3,9,1001,9,2,9,4,9,3,9,1002,9,2,9,4,9,99]


def generatePermutations(phaseSettings = range(0,5)):
	assert len(phaseSettings)== 5
	permutations = []
	
	for zero in phaseSettings:
		for one in phaseSettings:
			if one == zero:
				continue
			for two in phaseSettings:
				if two in [zero, one]:
					continue
				for three in phaseSettings:
					if three in [zero, one, two]:
						continue
					for four in phaseSettings:
						if four in [zero, one, two, three]:
							continue
						permutations.append([zero, one, two, three, four])					
	return permutations



def runComputerChain(comps, initialInput, phaseSettings = [], mode=0, verbose = False):
	assert mode < 2
	
	if mode==0:
		assert len(comps) == len(phaseSettings)
		lastOutput = initialInput
		for i in range (len(comps)):
			input = [phaseSettings[i], lastOutput]		
			if verbose:
				print('Starting amp '+str(i)+' with input '+str(input)+', lastOutput = '+str(lastOutput))

			output = comps[i].runProgram(input, mode, verbose)		

			assert len(output) == 1
			lastOutput = output[0]
			if verbose:
				print('output from amp '+str(i)+': '+ str(lastOutput))
		return lastOutput
	
	else:
		
		lastOutput = [initialInput]
		for i in range (len(comps)):
			if len(phaseSettings) == 0:
				input = lastOutput	#This may result in some extra (unused) inputs on the last go-round
			else:	#This is our first time, baby!
				input = [phaseSettings[i], lastOutput[0]]
			
			if verbose:
				print('Starting amp '+str(i)+' with input '+str(input)+', lastOutput = '+str(lastOutput))

			output = comps[i].runProgram(input, mode, verbose)		
		
			assert len(output) < 2
			if len(output)==1:	# Computer has not yet halted
				lastOutput = output
			elif(i == len(comps)-1):
				#	last comp of last iteration
				# The last go-round only lets us know that everything has halted.  lastOutput will not be updated
				#	on the last iteration
				return [lastOutput,99]
		return lastOutput


def runFeedbackLoop(comps, initialInput, phaseSettings, verbose):
	prevOutput = runComputerChain(comps, initialInput, phaseSettings, 1, verbose)	#Returns an int
	#print('completed first round')
	while len(prevOutput) == 1:
		#print('prevOutput is '+str(prevOutput))
		prevOutput = runComputerChain(comps, prevOutput[0], [], 1, verbose)	#Returns a list containing an int
	
	#On halt iteration, runComputerChain returns a list containing [99,finalOutput]
	return prevOutput[0]
	
	
	


testCode0 = [3,15,3,16,1002,16,10,16,1,16,15,15,4,15,99,0,0]	# best value is 43210 with sequence [4,3,2,1,0]
testCode1 = [3,23,3,24,1002,24,10,24,1002,23,-1,23,101,5,23,23,1,24,23,23,4,23,99,0,0]
testCode2 = [3,31,3,32,1002,32,10,32,1001,31,-2,31,1007,31,0,33,1002,33,7,33,1,33,31,31,1,32,31,31,4,31,99,0,0,0]

def doPartOne():
	v = False
	phases = generatePermutations()
	comps = []
	for i in range(5):
		comps.append(comp(ampCode))
		#comps.append(comp(testCode2))
		
	#runComputerChain(comps, [4,3,2,1,0], 0, v)
	#exit()
		
	
	best = 0
	print("Starting test over all amplifier phase configurations...")
	for phaseSettings in phases:
		#print("phase configuration "+str(config))
		result = runComputerChain(comps, 0, phaseSettings, 0, v)
		print('for sequence '+str(phaseSettings)+', result was '+str(result))
		best = max(best, result)
		#break
	print("Finished testing all phase configurations.  Best value was: "+str(best))

# best is 139629728 from [9,8,7,6,5]
feedbackTest0 = [3,26,1001,26,-4,26,3,27,1002,27,2,27,1,27,26,27,4,27,1001,28,-1,28,1005,28,6,99,0,0,5] 



def main():
	#doPartOne()
	#exit()
	v = False
	phases = generatePermutations(range(5,10))
	
	comps = []
	for i in range(5):
		comps.append(comp(ampCode.copy()))
		#comps.append(comp(feedbackTest0.copy()))
		

	#result = runFeedbackLoop(comps, 0, [5,6,7,8,9], True)
	#result = runFeedbackLoop(comps, 0, [9,8,7,6,5], True)
	#print(result)
	#exit()
	
	
	
	
	
	best = 0
	print("Starting feedback test over all amplifier phase configurations...")
	for phaseSettings in phases:
		#print("phase configuration "+str(config))
		for i,c in enumerate(comps):
			comps[i].resetState()
		result = runFeedbackLoop(comps, 0, phaseSettings, v)
		print('for sequence '+str(phaseSettings)+', result was '+str(result))
		best = max(best, result[0])
		#break
	print("Finished testing all phase configurations with feedback.  Best value was: "+str(best))

	
if __name__=="__main__":
	main()
