
w = 25
h = 6

layers = []

def numChars(str, c):
	count = 0
	for char in str:
		if char == c:
			count += 1
	return count

def doPartOne():
	file = open('day8input.txt')
	
	data = file.read()
	assert len(data) % (w*h) == 0
	
	start = 0
	end = w*h
	while end < len(data):
		layers.append(data[start:end])
		start += w*h
		end += w*h

	lowest = 6*25
	l = 0
		
	for i,layer in enumerate(layers):
		num = numChars(layer, '0')
		if num < lowest:
			lowest = min(num, lowest)
			l = i
	
	print(numChars(layers[l],'1')*numChars(layers[l],'2'))
	exit()
	

def main():
	file = open('day8input.txt')
	
	data = file.read()
	assert len(data) % (w*h) == 0
	
	start = 0
	end = w*h
	while end < len(data):
		layers.append(data[start:end])
		start += w*h
		end += w*h

	final = [-1]*(w*h)

	for l in layers:
		for i,p in enumerate(l):
			if final[i] != -1:
				continue
			elif p == '0':
				print(0)
				final[i] = '0'
			elif p == '1':
				print(1)
				final[i] = '1'
			elif p == '2':
				continue

	print(final)
	print("".join(final))
				

	lowest = 6*25
	l = 0
		
	for i,layer in enumerate(layers):
		num = numChars(layer, '0')
		if num < lowest:
			lowest = min(num, lowest)
			l = i
	
	print(numChars(layers[l],'1')*numChars(layers[l],'2'))
	exit()
	


if __name__=='__main__':
	main()