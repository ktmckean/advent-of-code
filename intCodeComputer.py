from enum import Enum

class computer:
	runMode = 0	#0 for single, 1 for feedback
	constMem = []
	mem = []
	pc = 0	#Global program counter
	rb = 0
	input = []
	inputIndex = 0
	verbose = False

	def __init__(self, software, input = []):
		self.constMem = software.copy()
		self.mem = software.copy()
		self.input = input
		
	def setInput(self, input):
		self.input = input
		self.inputIndex = 0

	def allocateMemory(self, newSize):
		newMem = [0]*newSize
		for i,val in enumerate(self.mem):
			newMem[i] = val
		self.mem = newMem


	def storeResult(self, val, dest):
		if dest >= len(self.mem):
			self.allocateMemory(2*dest)
		self.mem[dest] = val

	
	def getVal(self, address):
		if address >= len(self.mem):
			return 0
			#self.allocateMemory(2*address)
		return self.mem[address]



	#Address is the memory address of the argument 
	#	For mode 0, it contains the address at which to find the arg value
	#	For mode 1, it itself contains the arg value immediately
	#	For mode 2, it contains the offset from the relative base at which the arg value is located
	def getArg(self, address, mode):
		assert address < len(self.mem)
		if mode ==0:	#
			return self.getVal(self.mem[address])
			#return self.mem[self.mem[address]]
		elif mode ==1:
			return self.mem[address]
			#return self.mem[address]
		elif mode ==2:
			return self.getVal(self.rb + self.mem[address])
			#return self.mem[self.rb + self.mem[address]]
		else:
			print("Error: Mode must be 0 or 1 or 2.  Mode was "+str(mode))
			exit()
			
	def getWriteArg(self, address, mode):
		assert address < len(self.mem)
		if mode == 0:
			return self.mem[address]
		elif mode ==1:
			assert false
		elif mode == 2:
			return self.rb+self.mem[address]

	def getOpAndArgs(self):
		op = self.mem[self.pc]
		operation = op%100	#two digits
		arg1mode = int(op/100)%10
		arg2mode = int(op/1000)%10
		arg3mode = int(op/10000)%10	# never immediate
				
		if operation == 1 \
		or operation == 2 \
		or operation == 7 \
		or operation == 8:	#add, multiply, less than, equals
			assert self.pc+3 < len(self.mem)
			arg1 = self.getArg(self.pc+1, arg1mode)
			arg2 = self.getArg(self.pc+2, arg2mode)
			arg3 = self.getWriteArg(self.pc+3, arg3mode) # argModes work differently for writing	# Writing always uses the arg at self.pc+3 directly
			return [operation, arg1, arg2, arg3]
				
		#No more arguments for ops 3 and 4
		elif operation == 3:
			assert self.pc+1 < len(self.mem)
			# argModes work differently for writing
			arg1 = self.getWriteArg(self.pc+1, arg1mode)
			return [operation, arg1]

		elif operation == 4:
			assert self.pc+1 < len(self.mem)
			arg1 = self.getArg(self.pc+1, arg1mode)
			return [operation, arg1]
			
		elif operation == 5 or operation == 6:	#jump if true, jump if false
			assert self.pc+2 < len(self.mem)
			arg1 = self.getArg(self.pc+1, arg1mode)
			arg2 = self.getArg(self.pc+2, arg2mode)
			return [operation, arg1, arg2]
			
		elif operation == 9:
			arg1 = self.getArg(self.pc+1, arg1mode)
			return [operation, arg1]
		
		elif operation == 99:
			return [operation]
		
		else:
			print("Error: Operations 1-9 and 99 permitted.  Operation was "+str(op)) #str(operation))
			print(self.mem[:self.pc+4])

	def doAdd(self, op):
		self.storeResult(op[1]+op[2], op[3])
		self.pc += len(op)
	
	def doMultiply(self, op):
		self.storeResult(op[1]*op[2], op[3])
		self.pc += len(op)
	
	def doWrite(self, op):
		assert len(self.input) > self.inputIndex
		#	print('index: '+str(self.inputIndex)+', len: '+str(len(self.input))+', input: '+str(self.input))
		#	assert False
		if self.verbose:
			print('Writing value '+str(self.input[self.inputIndex])+' to address '+str(op[1]))
		self.storeResult(self.input[self.inputIndex], op[1])
		self.pc += len(op)
		self.inputIndex += 1
		#print('Value written. Address '+str(op[1])+' is now '+str(self.mem[op[1]]))
	def doOutput(self, op):
			self.pc += len(op)
			return [op[1]]

	def doJumpIfNotZero(self, op):
		if op[1] != 0:
			self.pc = op[2]
		else:
			self.pc += len(op)

	def doJumpIfZero(self, op):
		if op[1] == 0:
			self.pc = op[2]
		else:
			self.pc += len(op)

	def doLessThan(self, op):
		if op[1] < op[2]:
			self.storeResult(1, op[3])
			#self.mem[op[3]] = 1
		else:
			self.storeResult(0, op[3])
			#self.mem[op[3]] = 0
		self.pc += len(op)
		
	def doEquals(self, op):
		if op[1] == op[2]:
			self.storeResult(1, op[3])
		else:
			self.storeResult(0, op[3])
		self.pc += len(op)

	def doUpdateRelativeBase(self, op):
		if self.verbose:
			print('rb was '+str(self.rb)+', now is '+str(self.rb+op[1]))
		self.rb += op[1]
		self.pc += len(op)




	def executeOperation(self, op):
		output = []
		if op[0] == 1:
			self.doAdd(op)
		elif op[0] == 2:
			self.doMultiply(op)
		elif op[0] == 3:
			self.doWrite(op)
		elif op[0] == 4:
			output += self.doOutput(op)
		elif op[0] == 5:
			self.doJumpIfNotZero(op)
		elif op[0] == 6:
			self.doJumpIfZero(op)
		elif op[0] == 7:
			self.doLessThan(op)
		elif op[0] == 8:
			self.doEquals(op)
		elif op[0] == 9:
			self.doUpdateRelativeBase(op)
		elif op[0] == 99:
			return []
		
		return output	# All normal


	'''
	def executeOperation(self, op):
		output = []
		if op[0] == 1:
			self.mem[op[3]] = op[1]+op[2]
			self.pc += len(op)
		elif op[0] == 2:
			self.mem[op[3]] = op[1]*op[2]
			self.pc += len(op)
		elif op[0] == 3:
			assert len(self.input) > self.inputIndex
			self.mem[op[1]] = self.input[0]
			self.pc += len(op)
			self.inputIndex += 1
		elif op[0] == 4:
			output.append(op[1])
			self.pc += len(op)
		elif op[0] == 5:
			if op[1] != 0:
				self.pc = op[2]
			else:
				self.pc += len(op)
		elif op[0] == 6:
			if op[1] == 0:
				self.pc = op[2]
			else:
				self.pc += len(op)
		elif op[0] == 7:
			if op[1] < op[2]:
				self.mem[op[3]] = 1
			else:
				self.mem[op[3]] = 0
			self.pc += len(op)
		elif op[0] == 8:
			if op[1] == op[2]:
				self.mem[op[3]] = 1
			else:
				self.mem[op[3]] = 0
			self.pc += len(op)
		elif op[0] == 99:
			return []
			#exit()
			#output.append(self.mem[0])
			#self.pc += len(op)
		
		return output	# All normal
	'''
		
	def resetState(self):
		self.pc = 0
		self.inputIndex = 0
		self.mem = self.constMem.copy()
		
	#input should be a list of integers
	def runProgram(self, input, mode=0, verbose = False):
		self.runMode = mode
		if self.runMode == 0:	#single
			self.resetState()
		self.setInput(input)		
		self.verbose = verbose
		
		results = []
		
		while self.pc < len(self.mem):
			op = self.getOpAndArgs()
			if self.verbose:
				print('running... pc at '+str(self.pc)+', op = '+str(op))
			if op[0] == 99:
				return results
			
			output = self.executeOperation(op)
			
			if len(output) > 0:
				assert len(output)==1
				assert self.runMode < 2
				if self.runMode==0:	#single
					results.append(output[0])
					#print(results)
				else:
					if self.verbose:
						print("Pausing...")
					return output
				#print("output: "+str(output)+", pc at "+str(self.pc))
		return results