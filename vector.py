class vector:
	x = 0
	y = 0
	
	def __init__(self, x, y):
		self.x = x
		self.y = y
		
	def __str__(self):
		return str([self.x, self.y])
		
	def __add__(self, other):
		x = self.x + other.x
		y = self.y + other.y
		return vector(x,y)

	def __sub__(self, other):
		x = self.x - other.x
		y = self.y - other.y
		return vector(x,y)

	def __mod__(self, other):
		if self == other:
			return self
			
		if other.x == 0 and self.x == 0:
			xmod = 0
		else:
			xmod = self.x % other.x
			
		if other.y == 0 and self.y == 0:
			ymod = 0
		else:
			ymod = self.y % other.y
		return vector(xmod,ymod)
		
	def timesScalar(self, int):
		vec = vector(self.x, self.y)
		vec.x *= int
		vec.y *= int
		return vec
		
	def __gt__(self, other):
		return self.magSquared() > other.magSquared()

	def __lt__(self, other):
		return self.magSquared() < other.magSquared()
	
	def __eq__(self, other):
		return (self.x == other.x and self.y == other.y)

	def __ne__(self, other):
		return (self.x != other.x or self.y != other.y)
		
	def tup(self):
		return (self.x,self.y)
		
	def magSquared(self):
		return self.x**2 + self.y**2
		
	def mag(self):
		return math.sqrt(self.magSquared())
	
	def reduce(self):
		vec = vector(self.x, self.y)
		if abs(vec.x) == abs(vec.y):
			vec.x = 1 * sign(vec.x)
			vec.y = 1 * sign(vec.y)
		elif vec.x == 0:
			vec.y = 1 * sign(vec.y)	
		elif vec.y == 0:
			vec.x = 1 * sign(vec.x)
		else:
			factor = gcd(vec.x, vec.y)
			vec.x = int(vec.x / factor)
			vec.y = int(vec.y / factor)
		return vec
		
	def sharesSightLineWith(self, other):
		return self.reduce() == other.reduce()
		
	# Nice work but don't use this
	def isCollinearWith(self, other):
		return areCollinear(self, other)
		