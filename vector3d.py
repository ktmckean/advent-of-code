
# returns 1, -1, or 0
def sign(num):
	return int(num>0) - int(num<0)
	
def gcd(a, b, c=0):
	gcd = 1
	for num in range(1, max(abs(a)+1,abs(b)+1,abs(c)+1)):
		if a % num == 0 and b % num == 0 and c % num == 0:
			gcd = num
	return gcd

class vector3d:
	x = 0
	y = 0
	z = 0
	
	def __init__(self, x, y, z):
		self.x = x
		self.y = y
		self.z = z
		
	def __str__(self):
		return str([self.x, self.y, self.z])
		
	def __add__(self, other):
		x = self.x + other.x
		y = self.y + other.y
		z = self.z + other.z
		return vector3d(x,y,z)

	def __sub__(self, other):
		x = self.x - other.x
		y = self.y - other.y
		z = self.z - other.z
		return vector3d(x,y,z)

	def __div__(self, num):
		x = self.x / num
		y = self.y / num
		z = self.z / num
		return vector3d(x,y,z)

	#Unused in 3d.  Probably has some useful purpose
	def __mod__(self, other):
		if self == other:
			return self
			
		if other.x == 0 and self.x == 0:
			xmod = 0
		else:
			xmod = self.x % other.x
			
		if other.y == 0 and self.y == 0:
			ymod = 0
		else:
			ymod = self.y % other.y

		if other.z == 0 and self.z == 0:
			zmod = 0
		else:
			zmod = self.z % other.z
		return vector3d(xmod,ymod)
		
	def timesScalar(self, int):
		self.x *= int
		self.y *= int
		self.z *= int
		return vec
		
	def __gt__(self, other):
		return self.magSquared() > other.magSquared()

	def __lt__(self, other):
		return self.magSquared() < other.magSquared()
	
	def __eq__(self, other):
		return (self.x == other.x and self.y == other.y and self.z == other.z)

	def __ne__(self, other):
		return (self.x != other.x or self.y != other.y or self.z != other.z)

	def __getitem__(self, key):
		if key==0:
			return self.x
		elif key==1:
			return self.y
		elif key==2:
			return self.z
		else:
			raise IndexError

	def __setitem__(self, key, value):
		if key==0:
			self.x = value
		elif key==1:
			self.y = value
		elif key==2:
			self.z = value
		else:
			raise IndexError

	def __len__(self):
		return 3
		
	def tup(self):
		return (self.x,self.y,self.z)

	def max(self):
		return max(self.x,self.y,self.z)

	def min(self):
		return min(self.x,self.y,self.z)
		
	def magSquared(self):
		return self.x**2 + self.y**2 + self.z**2
		
	def mag(self):
		return math.sqrt(self.magSquared())
	
	#Could define a "reduce" to change state if useful
	def reduced(self):
		vec = vector3d(self.x, self.y, self.z)
		if abs(vec.x) == abs(vec.y) and abs(vec.x) == abs(vec.z):
			vec.x = 1 * sign(vec.x)
			vec.y = 1 * sign(vec.y)
			vec.z = 1 * sign(vec.z)			
		elif vec.x == 0 and vec.y == 0:
			vec.z = 1 * sign(vec.z)	
		elif vec.x == 0 and vec.z == 0:
			vec.y = 1 * sign(vec.y)
		elif vec.y == 0 and vec.z == 0:
			vec.x = 1 * sign(vec.x)
		else:
			factor = gcd(vec.x, vec.y, vec.z)
			vec.x = int(vec.x / factor)
			vec.y = int(vec.y / factor)
			vec.z = int(vec.z / factor)
		return vec
		
	def sharesSightLineWith(self, other):
		return self.reduce() == other.reduce()
		
	def signs(self):
		return vector3d(sign(self.x),sign(self.y),sign(self.z))
		
		
	# Nice work but don't use this
	def isCollinearWith(self, other):
		return areCollinear(self, other)
		